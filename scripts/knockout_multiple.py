import os
import knockout_fva
import argparse
from argparse import RawTextHelpFormatter
import time
from knockout_fva import *
from import_sbml_model import fix_model
from pathlib import Path


# -----------------------------------------------------------------------------
# Some parts of the code are taken from:
# https://github.com/ReScience-Archives/Mondeel-Ogundipe-Westerhoff-2018.git
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# Main knockout function
# -----------------------------------------------------------------------------
def main_knockout(model_import, proc, prog_bar, exch_react, ko_react, fraction_opt, eps, threshold,
                  always_unite, cleanSBML, reduce, recon3D, simlutaneous):
    start_time = time.time()
    print("Importing model...")
    # Model can be SBML: can be already cleaned (with blocked reactions removed etc.) or uncleaned
    fileformat = Path(model_import).suffix
    if fileformat == '.sbml' or fileformat == '.xml':
        print('Detected sbml model...')
        if cleanSBML:
            print("Fixing model...")
            model = fix_model(model_import, proc, prog_bar, recon3D)
        else:
            model = cobra.io.read_sbml_model(model_import)
            if not recon3D:
                for rxn in model.reactions:
                    if 'SUBSYSTEM' in rxn.notes:
                        rxn.subsystem = str(rxn.notes['SUBSYSTEM'])
    # Model can also be json file with pathways already correctly in the SUBSYSTEM attribute
    elif fileformat == '.json':
        print('Detected json model...')
        model = cobra.io.load_json_model(model_import)
        model = fix_genes(model)
    else:
        print('Please use a json or sbml file as your model.')

    model.solver = 'cplex'

    # Set the exchange reaction bounds
    rxnsOfInterest = []
    # If you provide no exchange reactions, then all of the model's exchange reactions are used
    if exch_react is None:
        if args.notjustexchanges:
            for i, rxn in enumerate(model.reactions):
                if not rxn.subsystem.startswith("Transport,") and not rxn.id.startswith("DM_") \
                        and not rxn.id.startswith("sink_") and not rxn.id.startswith("biomass_"):
                    rxnsOfInterest.append(rxn)
        else:
            rxnsOfInterest = model.exchanges

    # You can also provide a list in the console command: -x "EX_his_L_e EX_ile_L_e EX_leu_L_e EX_lys_L_e"
    else:
        if args.notjustexchanges:
            for r_ID in exch_react.split():
                rxnsOfInterest.append(model.reactions.get_by_id(r_ID))
        else:
            for r_ID in exch_react.split():
                rxnsOfInterest.append(model.exchanges.get_by_id(r_ID))
    # rxnsOfInterest = [rxn.id for rxn in rxnsOfInterest]
    # Set the exchange reaction bounds:
    for rxn in model.exchanges:
        rxn.lower_bound = -eps
    for rxn in model.exchanges:
        rxn.upper_bound = 1000

    # Create the list of reaction(s) or gene(s) to KO:
    # Needs to be a list of lists to enable multiple knockouts
    ids_to_knockout = []
    # Can be a file containing the IDs: new line means separate KO, space means simultaneous KOs
    # Example:
    # ACOAD1fm
    # ACOAD8m ACOAD9m
    # ACOAHi
    # ACOAO7p
    if os.path.isfile(ko_react):
        with open(ko_react, 'r') as ko_file:
            ids_to_knockout = [i.strip().split(' ') for i in ko_file]
    # If you provide no KO reactions or genes, then all of the model's reactions (apart from transport reactions,
    # exchange reactions, sinks and demand reactions) will be used
    elif ko_react is None:
        print("Using all reactions to KO... (may take a while)")
        if prog_bar:
            printProgressBar(0, len(model.reactions), prefix='Adding KO reactions:', suffix='Complete', length=50)
        for i, rxn in enumerate(model.reactions):
            if not rxn.subsystem.startswith("Transport,") and not rxn.id.startswith("DM_") \
                    and not rxn.id.startswith("sink_") and not rxn.id.startswith("biomass_"):
                if rxn not in model.exchanges:
                    ids_to_knockout.append(rxn.id)
            if prog_bar:
                printProgressBar(i + 1, len(model.reactions), prefix='Adding KO reactions:', suffix='Complete',
                                 length=50)
    # You can also provide the IDs directly as a console argument (multiple IDs separated by a space will be KO'd
    # separately)
    else:
        if simlutaneous:
            print("Using " + ko_react + " to KO simultaneously...")
            ids = ko_react.split()
            ids_to_knockout = [[i for i in ids]]
        else:
            print("Using " + ko_react + " to KO separately...")
            ids = ko_react.split()
            ids_to_knockout = [[i] for i in ids]

    # # Dataframe setup:
    # # Dataframe containing fluxes when the constrained reactions are forced forward:
    # WTf = pd.DataFrame(columns=['minimum', 'maximum'])
    # # Dataframe containing fluxes when the constrained reactions are forced backward:
    # WTb = pd.DataFrame(columns=['minimum', 'maximum'])
    # # Dataframe containing fluxes when the constrained reactions are KO'd:
    # mutant = pd.DataFrame(columns=['minimum', 'maximum'])

    fva_start_time = time.time()

    # Create the final biomarker table
    if not args.genes:
        biomarkerTable = pd.DataFrame(columns=['Reaction ID', 'Reaction', 'Pathway', 'Biomarkers'])
    else:
        biomarkerTable = pd.DataFrame(columns=['Gene ID', 'Reaction', 'Pathway', 'Biomarkers'])

    with model:
        print('Running ' + str(len(ids_to_knockout) * 3) + ' (' + str(
            len(ids_to_knockout)) + '*3) FVAs one after the other...')
        if prog_bar:
            printProgressBar(0, len(ids_to_knockout), prefix='Total FVA progress:', suffix='Complete',
                             length=50)
        for i, rxns in enumerate(ids_to_knockout):  # For 1 KO ID or 1 group of simultaneous KO reactions or genes:
            skip = False
            # Forward: change the bounds to force a forward flux in the reactions that go forward or are reversible:
            if not args.genes:
                rxns_rxn = [model.reactions.get_by_id(r_id) for r_id in rxns]
                rxns_to_calc_flux_for = rxnsOfInterest + rxns_rxn
            else:
                gene_rxns = []
                with model:
                    # To take into account the gene reaction rules, we must check the functionality of reactions
                    for gene_id in rxns:  # First loop to make all the genes non functional in the case of a multiple KO
                        gene = model.genes.get_by_id(gene_id)
                        gene.functional = False  # Render the gene(s) non functional
                    # Second loop to check for reaction functionality while all the genes are non functional
                    for gene_id in rxns:
                        for rxn in gene.reactions:
                            if not rxn.functional:  # Check if the associated reactions are functional or not
                                # If they're non functional, they depend on this gene so they need to be KO'd:
                                gene_rxns.append(rxn)
                    gene_rxns = list(set(gene_rxns))  # Remove duplicates
                if len(gene_rxns) == 0:
                    print("Knocking out " + gene_id + " will not have an effect on any reactions. Skipping..")
                    skip = True
                rxns_to_calc_flux_for = rxnsOfInterest + gene_rxns
            if not skip:
                if args.genes:
                    gene_rxns_ids = [r.id for r in gene_rxns]
                    WTf_all = knockout_fva.run_fva(model, 'forward', gene_rxns_ids, eps, rxns_to_calc_flux_for, proc,
                                                   fraction_opt, reduce, None)

                    # Backward: change the bounds to force a backward flux in the reactions that go backward or are reversible:
                    WTb_all = knockout_fva.run_fva(model, 'backward', gene_rxns_ids, eps, rxns_to_calc_flux_for, proc,
                                                   fraction_opt, reduce, None)

                    # Disease case (mutant)
                    if len(WTf_all) != 0:
                        mutant = knockout_fva.run_fva(model, 'mutant', gene_rxns_ids, eps, rxnsOfInterest, proc,
                                                      fraction_opt,
                                                      reduce, WTf_all)
                    elif len(WTb_all) != 0:
                        mutant = knockout_fva.run_fva(model, 'mutant', gene_rxns_ids, eps, rxnsOfInterest, proc,
                                                      fraction_opt,
                                                      reduce, WTb_all)

                    if prog_bar:
                        printProgressBar(i + 1, len(ids_to_knockout), prefix='Total FVA progress:', suffix='Complete',
                                         length=50)
                else:
                    WTf_all = knockout_fva.run_fva(model, 'forward', rxns, eps, rxns_to_calc_flux_for, proc,
                                                   fraction_opt, reduce, None)

                    # Backward: change the bounds to force a backward flux in the reactions that go backward or are reversible:
                    WTb_all = knockout_fva.run_fva(model, 'backward', rxns, eps, rxns_to_calc_flux_for, proc,
                                                   fraction_opt, reduce, None)

                    # Disease case (mutant)
                    if len(WTf_all) != 0:
                        mutant = knockout_fva.run_fva(model, 'mutant', rxns, eps, rxnsOfInterest, proc, fraction_opt,
                                                      reduce, WTf_all)
                    elif len(WTb_all) != 0:
                        mutant = knockout_fva.run_fva(model, 'mutant', rxns, eps, rxnsOfInterest, proc, fraction_opt,
                                                      reduce, WTb_all)

                    if prog_bar:
                        printProgressBar(i + 1, len(ids_to_knockout), prefix='Total FVA progress:', suffix='Complete',
                                         length=50)

                # Set the WT fluxes as the WTf ones, and extend the boundaries using WTb if necessary
                # (if always_unite == True)
                # Shlomi et al. did not do this: they kept WTf as WT, unless WTf = [0,0] (always_unite == False)
                rxnsIdsOfInterest = [r.id for r in rxnsOfInterest]

                WTf = WTf_all[WTf_all.index.isin(rxnsIdsOfInterest)]
                WTb = WTb_all[WTb_all.index.isin(rxnsIdsOfInterest)]

                WT = pd.DataFrame(columns=WTf.columns)
                if len(WTf) != 0 and len(WTb) == 0:  # No backward calculation, because backward FVA failed
                    WT = WTf
                elif len(WTb) != 0 and len(WTf) == 0:  # No forward calculation, because forward FVA failed
                    WT = WTb
                elif len(WTb) == 0 and len(WTf) == 0:  # No results
                    print('No healthy interval could be calculated')
                elif len(WTb) != 0 and len(WTf) != 0:  # If both FVAs worked
                    for rn in rxnsOfInterest:
                        r = rn.id
                        if always_unite:  # Using always_unite parameter
                            if r not in rxns:
                                WT.loc[r] = WTf.loc[r]
                                # Extend boundaries starting with WTf, and check to see if WTb is wider or not
                                if WTb.loc[r]['minimum'] < WTf.loc[r]['minimum']:
                                    WT.loc[r]['minimum'] = WTb.loc[r]['minimum']
                                if WTb.loc[r]['maximum'] > WTf.loc[r]['maximum']:
                                    WT.loc[r]['maximum'] = WTb.loc[r]['maximum']
                        else:  # Shlomi case
                            # Keep WTf unless it's 0
                            # print(r)
                            if r not in rxns:
                                if WTf.loc[r]["minimum"] == WTf.loc[r]["maximum"] == 0:
                                    WT.loc[r] = WTb.loc[r]
                                else:
                                    WT.loc[r] = WTf.loc[r]
                if args.write:
                    with open("test_out.csv", 'a') as f:
                        f.write('WT\n')
                        WT.to_csv(f)
                        f.write('mutant\n')
                        mutant.to_csv(f)
            if skip:
                WT = pd.DataFrame()
                mutant = pd.DataFrame()
            # Format the flux ranges to be more usable:
            WTint = {}
            mutantint = {}
            if len(WT) != 0 and len(mutant) != 0:
                for rn in rxnsOfInterest:
                    if rn.id not in rxns:
                        r = rn.id
                        WTint[r] = [round(WT.loc[r]["minimum"], 3), round(WT.loc[r]["maximum"], 3)]
                        mutantint[r] = [round(mutant.loc[r]["minimum"], 3), round(mutant.loc[r]["maximum"], 3)]

            # Calculate a score for each pair of flux ranges (WT and mutant):
            score = []  # Score to be compared to the score threshold
            flux_change = {}  # Flux change direction
            for r in rxnsOfInterest:  # For the one KO or for each reaction in the group of KOs
                if r.id not in rxns:
                    if len(WT) != 0 and len(mutant) != 0:
                        lb = [WTint[r.id][0], mutantint[r.id][0]]  # Store the two lower bounds (WT and mutant)
                        ub = [WTint[r.id][1], mutantint[r.id][1]]  # Store the two upper bounds (WT and mutant)
                        if lb == [0, 0]:
                            change_lower_bound = 0
                        else:
                            # Calculate the difference between the lower bounds divided by the biggest absolute lower bound
                            # Ex:
                            # WT = [-20, 50]
                            # mutant = [1, 30]
                            # change_lower_bound = abs(1 - 20) / 20 = 0.95
                            # change_upper_bound = abs(50 - 30) / 50 = 0.4
                            # ==> score will be 0.95
                            change_lower_bound = abs(max(lb) - min(lb)) / max([abs(el) for el in lb])
                        if ub == [0, 0]:
                            change_upper_bound = 0
                        else:
                            change_upper_bound = abs(max(ub) - min(ub)) / max([abs(el) for el in ub])
                    else:  # If none of the FVAs worked:
                        change_lower_bound = change_upper_bound = 0
                    score.append(max(change_lower_bound, change_upper_bound))  # Choose the max change as the score
                    if len(WT) != 0 and len(mutant) != 0:
                        # Determine direction of change
                        # If both lower bounds are the same, and both upper bounds are the same ==> no change
                        if WTint[r.id][0] == mutantint[r.id][0] and WTint[r.id][1] == mutantint[r.id][1]:
                            flux_change[r.id] = "(=)"
                        # If the WT upper bound is lower than the mutant lower bound ==> significant change
                        elif WTint[r.id][1] < mutantint[r.id][0]:
                            flux_change[r.id] = "(++)"
                        elif WTint[r.id][0] > mutantint[r.id][1]:
                            flux_change[r.id] = "(--)"
                        elif ((WTint[r.id][0] <= mutantint[r.id][0])
                              and (WTint[r.id][1] <= mutantint[r.id][1])
                              and (max(abs(WTint[r.id][0] - mutantint[r.id][0]),
                                       abs(WTint[r.id][1] - mutantint[r.id][1])) > 0)):
                            flux_change[r.id] = "(+)"
                        elif ((WTint[r.id][0] >= mutantint[r.id][0])
                              and (WTint[r.id][1] >= mutantint[r.id][1])
                              and (abs(WTint[r.id][0] - mutantint[r.id][0]) > 0
                                   or abs(WTint[r.id][1] - mutantint[r.id][1]) > 0)):
                            flux_change[r.id] = "(-)"
                        else:
                            flux_change[r.id] = "(?)"
            rxnsOfInterest = [r for r in rxnsOfInterest if r.id not in rxns]
            score_dict = dict(zip(rxnsOfInterest, score))

            biomarkerRxns = [r for r in score_dict if score_dict[r] > threshold]
            metabolite_list = []
            for r in biomarkerRxns:
                # ';' will separate each biomarker, and ' # ' will separate the biomarker from its change
                # Ex: (S)-3-sulfonatolactate(2-) # (-); taurine # (-)
                if not args.metabnames:
                    metab = '; '.join([next(iter(r.metabolites)).id]) + ' # ' + str(flux_change[r.id])
                else:
                    metab = '; '.join([next(iter(r.metabolites)).name]) + ' # ' + str(flux_change[r.id])
                metabolite_list.append(metab)
            if not args.genes:
                ids = sorted(rxns)
                reactions = [model.reactions.get_by_id(rxn).reaction for rxn in rxns]
                pathways = list(set([model.reactions.get_by_id(rxn).subsystem for rxn in rxns]))
            else:
                ids = sorted(rxns)
                reactions = list(set([r.id for gene_id in rxns
                                      for r in model.genes.get_by_id(gene_id).reactions]))
                pathways = list(set([model.reactions.get_by_id(rxn).subsystem for rxn in reactions]))
            biomarkerTable.loc[len(biomarkerTable)] = [' '.join(ids), ' '.join(reactions), ', '.join(pathways),
                                                       '; '.join(metabolite_list)]

    print('Significant biomarkers for each KOd reaction:')
    pd.set_option('display.max_columns', 4)  # Remove limit on column width
    if not args.genes:
        print(biomarkerTable[['Reaction ID', 'Biomarkers']].to_string())
    else:
        print(biomarkerTable[['Gene ID', 'Biomarkers']].to_string())
    fva_elapsed_time = time.time() - fva_start_time
    elapsed_time = time.time() - start_time
    print("Total elapsed time: {:.2f} sec".format(elapsed_time))
    print("Total FVA time: {:.2f} sec".format(fva_elapsed_time))
    print("Approximate FVA (x3) time per reaction KO: {:.2f} sec".format(
        fva_elapsed_time / len(ids_to_knockout)))
    return biomarkerTable


if __name__ == "__main__":
    description = "Calculates FVA results for a set of reactions (rxnsOfInterest) when another reaction " \
                  "(reactions_ids_to_knockout) is knocked out, for each input reaction\n"

    parser = argparse.ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)
    parser.add_argument("-m", "--model", help="Metabolic model (SBML or json).")
    parser.add_argument("-k", "--knockoutReactions", default=None,
                        help="List of reaction IDs to knockout separately. If empty, uses all reactions, apart from "
                             "exchange and transport reactions, to KO. Can be a file: IDs to be KO'd separately need "
                             "to be on a newline, and simultaneous KOs need to be on the same line separated by "
                             "spaces.")
    parser.add_argument('-g', '--genes', action='store_true', help="Use this flag to knockout genes instead of "
                                                                   "reactions (supply gene IDs instead of reaction IDs "
                                                                   "with -k).")
    parser.add_argument('--simultaneous', action='store_true', help="Use this flag to knockout IDs inputted via -k in "
                                                                    "the command line at the same time, rather than "
                                                                    "one after the other.")
    parser.add_argument("-x", "--exchangeReactions", default=None,
                        help="List of exchange reactions to measure fluxes for, separated by spaces. If empty, "
                             "uses all exchange reactions from the model.")
    parser.add_argument("-f", "--fractionOfOptimum", type=float, default=0,
                        help="The percentage of the optimum objective function to achieve.")
    parser.add_argument("-e", "--epsilon", type=int, default=10,
                        help="The flux value to force through the reactions to knockout in the WT case.")
    parser.add_argument("-t", "--threshold", type=float, default=0.1, help="Significance threshold for flux change "
                                                                           "between WT and KO conditions.")
    parser.add_argument("-u", "--unite", action='store_true',
                        help="Use this flag to unite the forward and backward WT flux ranges. Otherwise, it uses the "
                             "forward ranges.")
    parser.add_argument("-p", "--processors", type=int, default=4, help="Number of processors to use.")
    parser.add_argument("-s", "--separator", default=',', help="Separator to use in output file.")
    parser.add_argument("-o", "--outfile", help="Outfile path and name to write to.")
    parser.add_argument('-b', '--progressbar', action='store_true', help="Use this flag to show the progress bar in "
                                                                         "the console.")
    parser.add_argument("-c", "--cleanSBML", action='store_true',
                        help="Use this flag to clean the SBML file: imports using modified cobrapy "
                             "function, removes blocked reactions, links the NOTES section correctly, "
                             "links the subsystem from NOTES to the subsystem in the reaction object, "
                             "fixes name errors: _LPAREN_ _RPAREN_ instead of ( ).")
    parser.add_argument("-n", "--metabnames", action='store_true', help="Use this flag to use metabolite names instead "
                                                                        "of ids in the output table")
    parser.add_argument("-r", "--reduce", type=float, default=0,
                        help="Instead of KOing, use a value (0<r<1) to decrease the flux through the reaction in "
                             "the mutant case. For ex, a value of 0.3 will reduce the flux to 30% of the WT flux.")
    parser.add_argument("--recon3D", action='store_true', help="Use this flag to indicate that you're using Recon3D "
                                                               "(used for importing and cleaning model if sbml).")
    parser.add_argument("--write", action='store_true', help="Use this flag to the raw data (test)")
    parser.add_argument("--notjustexchanges", action='store_true', help="Use this flag to signal that you're inputting "
                                                                        "non-exchange reactions to be analysed")

    args = parser.parse_args()

    if args.reduce >= 1 or args.reduce < 0:
        raise ValueError(str(args.reduce) + " must be 0 <= r < 1.")
    biomarker_df = main_knockout(args.model, args.processors, args.progressbar, args.exchangeReactions,
                                 args.knockoutReactions, args.fractionOfOptimum,
                                 args.epsilon, args.threshold, args.unite, args.cleanSBML, args.reduce, args.recon3D,
                                 args.simultaneous)
    biomarker_df.to_csv(args.outfile, sep=args.separator, index=False)
