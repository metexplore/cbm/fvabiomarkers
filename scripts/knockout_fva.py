import libsbml
from copied import parse_legacy_sbml_notes, printProgressBar
import cobra
from cobra.flux_analysis import flux_variability_analysis
import pandas as pd


def link_pathways(filename, model):
    # Link subsystem (pathway) to reactions
    # Find a more efficient way to do this without loading the model again (even if it is fast)
    model_doc = libsbml.readSBML(filename)
    sbml_model = model_doc.getModel()
    sbml_reactions = sbml_model.getListOfReactions()
    reaction_note_dict = {}
    for sbml_reaction in sbml_reactions:
        reaction_note_dict[sbml_reaction.getId()] = parse_legacy_sbml_notes(sbml_reaction.getNotesString())
    for rxn in model.reactions:
        rxn.notes = reaction_note_dict["R_" + rxn.id]
    for rxn in model.reactions:
        if 'SUBSYSTEM' in rxn.notes:
            rxn.subsystem = str(rxn.notes['SUBSYSTEM'][0])
    return model


def fix_genes(model):
    for rxn in model.reactions:
        if 'GENE ASSOCIATION' in rxn.notes:
            rxn.gene_reaction_rule = rxn.notes['GENE ASSOCIATION'][0]
    return model


def remove_blocked_reactions(model, proc, progress):
    # Remove blocked reactions (reactions with 0 flux)
    print("Finding blocked reactions...")
    blocked = cobra.flux_analysis.find_blocked_reactions(model, processes=proc)
    if len(blocked) != 0:
        if progress:
            printProgressBar(0, len(blocked), prefix='Removing blocked reactions:', suffix='Complete', length=50)
        for i, r in enumerate(blocked):
            to_remove = model.reactions.get_by_id(r)
            to_remove.remove_from_model()
            if progress:
                printProgressBar(i + 1, len(blocked), prefix='Removing blocked reactions:', suffix='Complete',
                                 length=50)
    else:
        print("No blocked reactions could be found.")


def run_fva(model, case, rxns, eps, rxnsOfInterest, proc, fraction_opt, reduce, WT):
    if case == 'forward':
        with model:
            for r in rxns:  # Set all the reaction bounds for the forward FVA
                rxn = model.reactions.get_by_id(r)
                # Forward: change the bounds to force a forward flux in the reactions that go forward or are reversible:
                if rxn.upper_bound > 0:  # if [0, 1000] or [-1000, 1000]
                    model.reactions.get_by_id(rxn.id).lower_bound = eps  # / len(reactions_to_knockout)
                    # Bounds will be similar to [10, 1000]
                elif rxn.upper_bound == 0:  # if [-1000, 0]
                    model.reactions.get_by_id(rxn.id).upper_bound = -eps  # / len(reactions_to_knockout)
                    # Also force backward reactions if there are any => [-1000, -10]
            try:

                WTf = flux_variability_analysis(model, reaction_list=rxnsOfInterest, fraction_of_optimum=fraction_opt,
                                                processes=proc)
            except:
                rxnids = [rn for rn in rxns]
                s = ' '.join(rxnids)
                if len(s) > 48:
                    s = s[:48] + '..'
                print('Forward FVA could not be solved for ' + s + '. Continuing without the forward '
                                                                   'interval.')
                WTf = pd.DataFrame()
            return WTf
    elif case == 'backward':
        with model:
            for r in rxns:  # Set all the reaction bounds for the backward FVA
                rxn = model.reactions.get_by_id(r)
                # Backward: change the bounds to force a backward flux in the reactions that go backward or are
                # reversible:
                if rxn.lower_bound < 0:  # if [-1000, 0] or [-1000, 1000]
                    model.reactions.get_by_id(rxn.id).lower_bound = -eps  # / len(reactions_to_knockout)
                    # Bounds will be similar to [-1000, -10]
                elif rxn.lower_bound == 0:  # if [0, 1000]
                    model.reactions.get_by_id(rxn.id).lower_bound = eps  # / len(reactions_to_knockout)
                    # Also force forward reactions if there are any => [10, 1000]
            try:
                WTb = flux_variability_analysis(model, reaction_list=rxnsOfInterest, fraction_of_optimum=fraction_opt,
                                                processes=proc)
            except:
                rxnids = [rn for rn in rxns]
                s = ' '.join(rxnids)
                if len(s) > 48:
                    s = s[:48] + '..'
                print('Backward FVA could not be solved for ' + s + '. Continuing without the backward '
                                                                    'interval.')
                WTb = pd.DataFrame()
            return WTb
    elif case == 'mutant':
        # Disease case (mutant)
        # KO reactions by setting the bounds to 0
        with model:
            if reduce == 0:
                for r in rxns:  # Set all the reaction bounds to 0 for the mutant FVA
                    rxn = model.reactions.get_by_id(r)
                    model.reactions.get_by_id(rxn.id).lower_bound = model.reactions.get_by_id(rxn.id).upper_bound = 0
                mutant = flux_variability_analysis(model, reaction_list=rxnsOfInterest,
                                                   fraction_of_optimum=fraction_opt,
                                                   processes=proc)
            else:
                for r in rxns:
                    # calculate new bounds
                    # Check sign of WT bounds:
                    rxn = model.reactions.get_by_id(r)
                    pd.set_option('display.max_rows', 1000)
                    old_ub = WT.loc[rxn.id][1]
                    old_lb = WT.loc[rxn.id][0]
                    # Reduce all the reaction bounds for the mutant backward FVA:
                    if old_ub < 0:  # Ex: [-20, -5]
                        new_lb = old_lb - ((old_ub - old_lb) * reduce)  # -20 - ((-20+5)*0.5) = - 12.5
                        new_ub = old_ub  # No difference
                        model.reactions.get_by_id(rxn.id).upper_bound = new_ub
                        model.reactions.get_by_id(rxn.id).lower_bound = new_lb
                        try:
                            mutantb = flux_variability_analysis(model, reaction_list=rxnsOfInterest,
                                                                fraction_of_optimum=fraction_opt, processes=proc)
                        except:
                            rxnids = [rn for rn in rxns]
                            s = ' '.join(rxnids)
                            if len(s) > 48:
                                s = s[:48] + '..'
                            print('Backward mutant FVA could not be solved for ' + s + '. Continuing without the '
                                                                                       'Backward mutant interval.')
                            mutantb = pd.DataFrame()
                        mutant = mutantb
                    # Reduce all the reaction bounds for the mutant forward FVA:
                    elif old_lb > 0:  # Ex: [100, 500]
                        new_lb = old_lb  # No difference
                        new_ub = old_ub - ((old_ub - old_lb) * reduce)  # 500 - ((500-100)*0.5) = 300
                        model.reactions.get_by_id(rxn.id).upper_bound = new_ub
                        model.reactions.get_by_id(rxn.id).lower_bound = new_lb
                        try:
                            mutantf = flux_variability_analysis(model, reaction_list=rxnsOfInterest,
                                                                fraction_of_optimum=fraction_opt, processes=proc)

                        except:
                            rxnids = [rn for rn in rxns]
                            s = ' '.join(rxnids)
                            if len(s) > 48:
                                s = s[:48] + '..'
                            print('Forward mutant FVA could not be solved for ' + s + '. Continuing without the '
                                                                                      'forward mutant interval.')
                            mutantf = pd.DataFrame()
                        mutant = mutantf
                    # Impossible because WTf or WTb are strictly either positive or negative:
                    # elif old_lb == 0 and old_ub == 0:
                    #     new_ub = 0
                    #     new_lb = 0
                    # else:  # Ex: [-50, 10]
                    #     new_lb = old_lb - ((old_ub - abs(old_lb)) * reduce)/2  # -50 - ((10-50)*0.5)/2 = -40
                    #     new_ub = old_ub - ((old_ub - abs(old_lb)) * reduce)/2  # 10 - ((10-50)*0.5)/2 = 0

                # Keep mutantf unless it's 0
                # mutant = pd.DataFrame(columns=mutantf.columns)
                # if len(mutantf) != 0 and len(mutantb) == 0:  # No backward calculation, because backward FVA failed
                #     mutant = mutantf
                # elif len(mutantb) != 0 and len(mutantf) == 0:  # No forward calculation, because forward FVA failed
                #     mutant = mutantb
                # elif len(mutantb) == 0 and len(mutantf) == 0:  # No results
                #     print('No healthy interval could be calculated')
                # elif len(mutantb) != 0 and len(mutantf) != 0:  # If both FVAs worked
                #     for rn in rxnsOfInterest:
                #         r = rn.id
                #         if mutantf.loc[r]["minimum"] == mutantf.loc[r]["maximum"] == 0:
                #             mutant.loc[r] = mutantb.loc[r]
                #         else:
                #             mutant.loc[r] = mutantf.loc[r]
            return mutant
