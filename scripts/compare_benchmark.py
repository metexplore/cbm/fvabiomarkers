import itertools
import numpy as np
import pandas as pd
import argparse
from argparse import RawTextHelpFormatter

description = "Imports a csv file and compares it to another file containing a reference list of metabolites.\n"

parser = argparse.ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)
parser.add_argument("-i", "--input", help="Input csv file to compare")
parser.add_argument("-r", "--ref", help="Input csv file to use as reference")
parser.add_argument("-s", "--sep", default=',', help="Separator for input ref file")

args = parser.parse_args()

data = pd.read_csv(args.input)
ref = pd.read_csv(args.ref, header=None, sep=args.sep)
pd.set_option('display.max_columns', 22)
cols = [1, 2]
data.drop(data.columns[cols], axis=1, inplace=True)

bm = data.iloc[:, 1].values[0]
only_names = []
if not pd.isnull(bm):
    temp = bm.split('; ')
else:
    temp = []
for item in temp:
    only_names.append(item.split(' # ')[0])
tp = 0
fp = 0
fn = 0

data_set = set([str.casefold(name) for name in only_names])
ref_set = set([str.casefold(name) for name in ref.iloc[0].values])
print("Extract of input data set:")
print(set(itertools.islice(data_set, 10)))
print("Reference set:")
print(ref_set)
in_common = data_set.intersection(ref_set)
tp = len(in_common)
fp = len(data_set.difference(ref_set))
fn = len(ref_set.difference(data_set))
if tp + fp == 0:
    precision = 0
else:
    precision = tp / (tp + fp)
if tp + fn == 0:
    recall = 0
else:
    recall = tp / (tp + fn)
print("Metabolites in common:")
print(tp)
print(in_common)
print("Precision:")
print(precision)
print("Recall:")
print(recall)

