import pandas as pd
import argparse
from argparse import RawTextHelpFormatter
import os.path

description = "Imports a precision and recall file and formats it for easier usage.\n"

parser = argparse.ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)
parser.add_argument("-i", "--input", help="Input precision and recall filename")

args = parser.parse_args()

data = pd.read_csv(args.input, header=None)


def parse_row(r):
    split_row = r.split("_")
    fr_of_opt = split_row[2]
    thr = split_row[4][:-4]
    return thr, fr_of_opt


thr_list = []
fr_of_opt_list = []
precision_list = []
recall_list = []
tp_list = []

for index, row in data.iterrows():
    if row[0].endswith(".csv"):
        t, f = parse_row(row[0])
        thr_list.append(t)
        fr_of_opt_list.append(f)
    if row[0].startswith("Precision:"):
        precision = row[0].split()[1]
        precision_list.append(precision)
    if row[0].startswith("Recall:"):
        recall = row[0].split()[1]
        recall_list.append(recall)
    if row[0].startswith("Number of TP:"):
        tp = row[0].split()[3]
        tp_list.append(tp)

index_df = pd.DataFrame({"Fraction of optimum": fr_of_opt_list, "Threshold": thr_list})
table = pd.DataFrame({"Precision": precision_list, "Recall": recall_list, "Number of TP": tp_list})
table.index = pd.MultiIndex.from_arrays(index_df.values.T, names=['fraction of optimum', 'threshold'])

new_index_array = [['0', '0', '0.9', '0.9', '0.9', '0.9', '0.9', '0.9', '0.9', '0.1', '0.5'],
                   ['0', '0.1', '0', '0.01', '0.03', '0.05', '0.1', '0.15', '0.2', '0.1', '0.1']]
new_idx = pd.MultiIndex.from_arrays(new_index_array)

table = table.reindex(new_idx)
print(table)
table.to_csv("out/out.csv")
