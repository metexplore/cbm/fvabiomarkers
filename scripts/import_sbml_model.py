from knockout_fva import link_pathways, remove_blocked_reactions
from read_sbml_model_no_boundary_reactions import read_sbml_model
import argparse
from argparse import RawTextHelpFormatter
import cobra


def fix_model(modfile, proc, bar, recon3D):
    model_import = read_sbml_model(modfile)
    model_import.solver = 'cplex'
    if not recon3D:
        # Link the pathways (subsystem) in the model as they are stored in NOTES (this also links all the NOTES)
        model = link_pathways(modfile, model_import)

        # Delete the blocked reactions (reactions with 0 flux) from the model
        remove_blocked_reactions(model, proc, bar)

        # Fix name errors:
        for r in model.reactions:
            if '_LPAREN_' in r.id:
                s = r.id.replace('_LPAREN', '')
                s = s.replace('_RPAREN_', '')
                r.id = s
        model.repair()  # Rebuilds indexes after having renamed reactions

    else:
        model = model_import
    return model


if __name__ == "__main__":
    description = "Imports a SBML model (RECON2.2) and modifies and fixes it to be used in FVA analyses\n"

    parser = argparse.ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)
    parser.add_argument("-m", "--model", help="model")
    parser.add_argument("-j", "--jsonmodel", action='store_true', help="Use this flag to use a json model as input")
    parser.add_argument("-p", "--processors", type=int, default=4, help="Number of processors to use")
    parser.add_argument("-o", "--outfile", help="Path and name of the new model file")
    parser.add_argument('-b', '--progressbar', action='store_true', help="Use this flag to show the progress bar in "
                                                                         "the console")
    parser.add_argument('-f', '--format', choices=['sbml','json'], help="Output file format. If outputting sbml, "
                                                                        "the pathway susbsystem will not be retained.")
    parser.add_argument('--recon3d', action='store_true', help="Use this flag to use recon3D")
    args = parser.parse_args()

    if args.jsonmodel:
        mod = cobra.io.load_json_model(args.model)
        mod.solver = 'cplex'
        remove_blocked_reactions(mod, args.processors, args.progressbar)
    else:
        mod = fix_model(args.model, args.processors, args.progressbar, args.recon3d)
    if args.format == 'sbml':
        print("Writing to sbml file... Please note that pathway subsystem information will not be saved to this model.")
        cobra.io.write_sbml_model(mod, args.outfile)
    elif args.format == 'json':
        print("Writing to json file...")
        cobra.io.save_json_model(mod, args.outfile)
