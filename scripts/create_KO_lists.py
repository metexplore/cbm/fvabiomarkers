# From a SBML model, create a list of KO reaction IDs (excluding transport reactions and exchange reactions)
import argparse
from copied import printProgressBar
from knockout_fva import remove_blocked_reactions
from read_sbml_model_no_boundary_reactions import read_sbml_model
import cobra
from pathlib import Path

description = "Splits all the reactions, apart from transport and exchange, into the chosen number of files."

parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("-m", "--model", help="Metabolic model")
parser.add_argument("-n", "--number_of_outfiles", type=int, help="The number of IDs per file.")
parser.add_argument('-b', '--progressbar', action='store_true', help="Use this flag to show the progress bar in "
                                                                     "the console")
parser.add_argument("-o", "--outfileprefix", help="File path and file prefix of outfile names")
parser.add_argument("-p", "--processors", type=int, help="Number of processors")
parser.add_argument("-c", "--cleanSBML", action='store_true',
                    help="Use this flag to clean up the SBML file: imports using modified cobrapy "
                         "function, removes blocked reactions, links the NOTES section correctly, "
                         "links the subsystem from NOTES to the subsystem in the reaction object, "
                         "fixes name errors: _LPAREN_ _RPAREN_ instead of ( ).")
parser.add_argument('-i', '--inputformat', default='sbml', choices=['sbml', 'json'], help="Input file format.")
args = parser.parse_args()


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for j in range(0, len(lst), n):
        yield lst[j:j + n]


inputformat = Path(args.model).suffix
if inputformat == '.sbml':
    print('Detected sbml model...')
    model = read_sbml_model(args.model)
elif inputformat == '.json':
    print('Detected json model...')
    model = cobra.io.load_json_model(args.model)

model.solver = 'cplex'

if args.cleanSBML:
    print("Removing blocked reactions...")
    remove_blocked_reactions(model, args.processors, args.progressbar)
reactions_ids_to_knockout = []

if args.progressbar:
    printProgressBar(0, len(model.reactions), prefix='Adding KO reactions:', suffix='Complete', length=50)
else:
    print("Adding all KO reaction IDs...")
for i, rxn in enumerate(model.reactions):
    # This may miss un-annotated transport reactions but it removes a good number of transport reactions
    if not rxn.subsystem.startswith("Transport,") and not rxn.id.startswith("DM_") \
                    and not rxn.id.startswith("sink_"):
        if rxn not in model.exchanges:
            reactions_ids_to_knockout.append(rxn.id)
    if args.progressbar:
        printProgressBar(i + 1, len(model.reactions), prefix='Adding KO reactions:', suffix='Complete',
                         length=50)
print("Writing to files...")
ko_chunks = list(chunks(reactions_ids_to_knockout, args.number_of_outfiles))
for i, ch in enumerate(ko_chunks):
    with open(str(args.outfileprefix) + str(i) + ".txt", 'w') as chunk_out:
        for r_id in ch:
            chunk_out.write(str(r_id) + "\n")
