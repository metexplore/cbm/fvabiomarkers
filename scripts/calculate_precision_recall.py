import pandas as pd
import argparse
from argparse import RawTextHelpFormatter
import os.path
description = "Imports a FVA csv results file and calculates precision and recall.\n"

parser = argparse.ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)
parser.add_argument("-i", "--input", help="Input csv table filename")
parser.add_argument("-m", "--model", choices=['recon1', 'recon2v2', 'recon3d', 'human1'], help="Model version")
parser.add_argument("-g", "--genes", action="store_true", help="Use this flag to use output from knockout_multiple "
                                                               "after using genes")
args = parser.parse_args()


def unique(L):
    seen = set()
    for item in L:
        if item not in seen:
            seen.add(item)
            yield item


in_table = pd.read_csv(args.input)
all_biomarkers = []
for bm in in_table['Biomarkers'][in_table['Biomarkers'].notnull()]:
    only_names = []
    temp = bm.split('; ')
    for item in temp:
        only_names.append(item.split(' # ')[0])
    all_biomarkers.append(only_names)
all_biomarkers_split = [item for sublist in all_biomarkers for item in sublist]
all_biomarkers_split[:] = unique(all_biomarkers_split)
if len(all_biomarkers_split) != 20:
    if args.model == 'recon2v2':
        aa_dict = {'cys_L_e': 'L-Cysteine', 'phe_L_e': 'L-Phenylalanine', 'tyr_L_e': 'L-Tyrosine',
                   'his_L_e': 'L-Histidine',
                   'met_L_e': 'L-Methionine', 'ile_L_e': 'L-Isoleucine', 'leu_L_e': 'L-Leucine',
                   'lys_L_e': 'L-Lysine',
                   'thr_L_e': 'L-Threonine', 'trp_L_e': 'L-Tryptophan', 'val_L_e': 'L-Valine',
                   'arg_L_e': 'L-Arginine',
                   'gln_L_e': 'L-Glutamine', 'asn_L_e': 'L-Asparagine', 'glu_L_e': 'L-Glutamate',
                   'gly_e': 'Glycine',
                   'ser_L_e': 'L-Serine', 'pro_L_e': 'L-Proline', 'asp_L_e': 'L-Aspartate',
                   'ala_L_e': 'L-Alanine'
                   }
    elif args.model == 'recon3d' or args.model == 'recon1':
        aa_dict = {'cys__L_e': 'L-Cysteine', 'phe__L_e': 'L-Phenylalanine', 'tyr__L_e': 'L-Tyrosine',
                   'his__L_e': 'L-Histidine',
                   'met__L_e': 'L-Methionine', 'ile__L_e': 'L-Isoleucine', 'leu__L_e': 'L-Leucine',
                   'lys__L_e': 'L-Lysine',
                   'thr__L_e': 'L-Threonine', 'trp__L_e': 'L-Tryptophan', 'val__L_e': 'L-Valine',
                   'arg__L_e': 'L-Arginine',
                   'gln__L_e': 'L-Glutamine', 'asn__L_e': 'L-Asparagine', 'glu__L_e': 'L-Glutamate',
                   'gly_e': 'Glycine',
                   'ser__L_e': 'L-Serine', 'pro__L_e': 'L-Proline', 'asp__L_e': 'L-Aspartate',
                   'ala__L_e': 'L-Alanine'
                   }
    elif args.model == 'human1':
        aa_dict = {'cysteine': 'L-Cysteine', 'phenylalanine': 'L-Phenylalanine', 'tyrosine': 'L-Tyrosine',
                   'histidine': 'L-Histidine',
                   'methionine': 'L-Methionine', 'isoleucine': 'L-Isoleucine', 'leucine': 'L-Leucine',
                   'lysine': 'L-Lysine',
                   'threonine': 'L-Threonine', 'tryptophan': 'L-Tryptophan', 'valine': 'L-Valine',
                   'arginine': 'L-Arginine',
                   'glutamine': 'L-Glutamine', 'asparagine': 'L-Asparagine', 'glutamate': 'L-Glutamate',
                   'glycine': 'Glycine',
                   'serine': 'L-Serine', 'proline': 'L-Proline', 'aspartate': 'L-Aspartate',
                   'alanine': 'L-Alanine'}
    for aa in aa_dict.keys():
        if aa not in all_biomarkers_split:
            all_biomarkers_split.append(aa)

data = pd.DataFrame(columns=['Reaction ID', 'Biomarkers'] + all_biomarkers_split)
if not args.genes:
    data['Reaction ID'], data['Biomarkers'] = in_table['Reaction ID'], in_table['Biomarkers']
else:
    data['Reaction ID'], data['Biomarkers'] = in_table['Reaction'], in_table['Biomarkers']
data.set_index('Reaction ID', inplace=True)

if not data.isnull().all().all():  # Check if not completely empty
    for rxn, row in data.iterrows():
        bm_list = []
        if type(data.loc[[rxn], ['Biomarkers']].values[0][0]) != float:
            bm_list = (data.loc[[rxn], ['Biomarkers']].values[0][0]).split('; ')
        else:
            bm_list = ['']
        change_list = []
        for i, bm in enumerate(bm_list):
            if bm != '':
                bm_list[i] = bm.split(' # ')[0]
                if (bm.split(' # ')[1]) == '(-)':
                    change_list.append("-1")
                elif (bm.split(' # ')[1]) == '(+)':
                    change_list.append("+1")
                # Can be equal to (--) or (++)
                elif (bm.split(' # ')[1]) == '(--)':
                    change_list.append("-1")
                elif (bm.split(' # ')[1]) == '(++)':
                    change_list.append("+1")
                else:
                    change_list.append('00')
        for i, bm in enumerate(bm_list):
            if bm != '':
                for column in data.columns[1:]:
                    if column.casefold() == bm.casefold():
                        data.at[rxn, column] = change_list[i]

data.drop('Biomarkers', 1, inplace=True)
data.fillna("00", inplace=True)

if args.model == 'recon2v2':
    aa_tri_dict = {'cys_L_e': 'Cys', 'phe_L_e': 'Phe', 'tyr_L_e': 'Tyr', 'his_L_e': 'His',
                   'met_L_e': 'Met', 'ile_L_e': 'Iso', 'leu_L_e': 'Leu', 'lys_L_e': 'Lys',
                   'thr_L_e': 'Thr', 'trp_L_e': 'Trp', 'val_L_e': 'Val', 'arg_L_e': 'Arg',
                   'gln_L_e': 'Gln', 'asn_L_e': 'Asn', 'glu_L_e': 'Glu', 'gly_e': 'Gly',
                   'ser_L_e': 'Ser', 'pro_L_e': 'Pro', 'asp_L_e': 'Asp', 'ala_L_e': 'Ala'
                   }
elif args.model == 'recon3d' or args.model == 'recon1':
    aa_tri_dict = {'cys__L_e': 'Cys', 'phe__L_e': 'Phe', 'tyr__L_e': 'Tyr', 'his__L_e': 'His',
                   'met__L_e': 'Met', 'ile__L_e': 'Iso', 'leu__L_e': 'Leu', 'lys__L_e': 'Lys',
                   'thr__L_e': 'Thr', 'trp__L_e': 'Trp', 'val__L_e': 'Val', 'arg__L_e': 'Arg',
                   'gln__L_e': 'Gln', 'asn__L_e': 'Asn', 'glu__L_e': 'Glu', 'gly_e': 'Gly',
                   'ser__L_e': 'Ser', 'pro__L_e': 'Pro', 'asp__L_e': 'Asp', 'ala__L_e': 'Ala'
                   }
elif args.model == 'human1':
    aa_tri_dict = {'cysteine': 'Cys', 'phenylalanine': 'Phe', 'tyrosine': 'Tyr', 'histidine': 'His',
                   'methionine': 'Met', 'isoleucine': 'Iso', 'leucine': 'Leu', 'lysine': 'Lys',
                   'threonine': 'Thr', 'tryptophan': 'Trp', 'valine': 'Val', 'arginine': 'Arg',
                   'glutamine': 'Gln', 'asparagine': 'Asn', 'glutamate': 'Glu', 'glycine': 'Gly',
                   'serine': 'Ser', 'proline': 'Pro', 'aspartate': 'Asp', 'alanine': 'Ala'}
for aa in data.columns:
    data.rename(columns={aa: aa_tri_dict[aa]}, inplace=True)

column_order = ['Arg', 'Leu', 'Phe', 'Cys', 'Gln', 'Ser',
                'Asn', 'Trp', 'Pro', 'Thr', 'Asp', 'Gly',
                'Glu', 'Iso', 'Lys', 'Val', 'Met', 'Tyr',
                'Ala', 'His']
data = data[column_order]
pd.set_option('display.max_columns', 22)

# Sort indexes by alphabetical order for easier precision and recall calculations
for index, row in data.iterrows():
    split_index = index.split()
    split_index.sort()
    new_index = ' '.join(split_index)
    data.rename(index={index: new_index}, inplace=True)

omim = pd.DataFrame().reindex_like(data)

if args.model == 'recon1':
    if not args.genes:
        omim.loc['AHCi', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['CYSTSERex SERLYSNaex', 'Arg'] = omim.loc['CYSTSERex SERLYSNaex', 'Lys'] = \
            omim.loc['SERLYSNaex', 'Arg'] = omim.loc['SERLYSNaex', 'Lys'] = omim.loc['FTCD GluForTx', 'His'] = \
            omim.loc['HISDr', 'His'] = omim.loc['CYSTS METS MTHFR3', 'Met'] = omim.loc['PRO1xm PROD2m', 'Pro'] = \
            omim.loc['OIVD1m OIVD2m OIVD3m', 'Leu'] = omim.loc['OIVD1m OIVD2m OIVD3m', 'Iso'] = \
            omim.loc['OIVD1m OIVD2m OIVD3m', 'Val'] = omim.loc['METAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX2', 'Phe'] = omim.loc['DHPR', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm', 'Gly'] = '+'
        omim.loc['PHETHPTOX2', 'Tyr'] = '-'
    else:
        omim.loc['AHCi SEAHCYSHYD', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['AMY1e AMY2e CYSTSERex', 'Arg'] = omim.loc['AMY1e AMY2e CYSTSERex', 'Lys'] = \
            omim.loc['AMY1e AMY2e CYSTSERex SERLYSNaex', 'Arg'] = \
            omim.loc['AMY1e AMY2e CYSTSERex SERLYSNaex', 'Lys'] = \
            omim.loc['SERLYSNaex', 'Arg'] = omim.loc['SERLYSNaex', 'Lys'] = omim.loc['FTCD GluForTx', 'His'] = \
            omim.loc['HISDr', 'His'] = omim.loc['CYSTS SELCYSTS', 'Met'] = omim.loc['PRO1xm PROD2m', 'Pro'] = \
            omim.loc['2OXOADOXm AKGDm GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm OIVD1m OIVD2m OIVD3m PDHm', 'Leu'] = \
            omim.loc['2OXOADOXm AKGDm GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm OIVD1m OIVD2m OIVD3m PDHm', 'Iso'] = \
            omim.loc['2OXOADOXm AKGDm GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm OIVD1m OIVD2m OIVD3m PDHm', 'Val'] = \
            omim.loc['METAT SELMETAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX2', 'Phe'] = omim.loc['DHPR', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR PPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm', 'Gly'] = '+'
        omim.loc['PHETHPTOX2', 'Tyr'] = '-'
elif args.model == 'recon3d':
    if not args.genes:
        omim.loc['AHCi', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['CYSTSERex SERLYSNaex', 'Arg'] = omim.loc['CYSTSERex SERLYSNaex', 'Lys'] = \
            omim.loc['SERLYSNaex', 'Arg'] = omim.loc['SERLYSNaex', 'Lys'] = omim.loc['FTCD GluForTx', 'His'] = \
            omim.loc['HISDr', 'His'] = omim.loc['CYSTS METS MTHFR3', 'Met'] = omim.loc['PRO1x PROD2m', 'Pro'] = \
            omim.loc['OIVD1m OIVD2m OIVD3m', 'Leu'] = omim.loc['OIVD1m OIVD2m OIVD3m', 'Iso'] = \
            omim.loc['OIVD1m OIVD2m OIVD3m', 'Val'] = omim.loc['METAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX2', 'Phe'] = omim.loc['DHPR', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm', 'Gly'] = '+'
        omim.loc['PHETHPTOX2', 'Tyr'] = '-'
    else:
        omim.loc['AHCi', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['AMY1e AMY2e CYSTALArBATtc CYSTLEUrBATtc CYSTSERex ORNALArBATtc ORNLEUrBATtc r0946 r1992 r1993 r1994 r1995 r1996 r1997 r1998 r1999 r2000 r2001 r2002 r2003 r2004 r2005 r2006 r2007 r2008 r2009 r2010 r2011 r2012 r2013 r2014 r2015 r2016 r2017 r2018 r2019 r2020 r2021 r2022 r2023 r2024 r2026 r2027 r2028 r2029 r2030 r2031 r2032 r2034 r2035 r2036 r2037 r2038 r2039 r2040 r2041 r2042 r2043 r2044 r2045 r2046 r2047 r2048 r2049 r2050 r2051 r2052 r2053 r2054 r2055 r2056 r2057 r2058 r2059 r2060 r2061 r2062 r2063 r2064 r2065 r2066 r2067 r2068 r2069 r2070 r2071', 'Arg'] = \
            omim.loc['AMY1e AMY2e CYSTALArBATtc CYSTLEUrBATtc CYSTSERex ORNALArBATtc ORNLEUrBATtc r0946 r1992 r1993 r1994 r1995 r1996 r1997 r1998 r1999 r2000 r2001 r2002 r2003 r2004 r2005 r2006 r2007 r2008 r2009 r2010 r2011 r2012 r2013 r2014 r2015 r2016 r2017 r2018 r2019 r2020 r2021 r2022 r2023 r2024 r2026 r2027 r2028 r2029 r2030 r2031 r2032 r2034 r2035 r2036 r2037 r2038 r2039 r2040 r2041 r2042 r2043 r2044 r2045 r2046 r2047 r2048 r2049 r2050 r2051 r2052 r2053 r2054 r2055 r2056 r2057 r2058 r2059 r2060 r2061 r2062 r2063 r2064 r2065 r2066 r2067 r2068 r2069 r2070 r2071', 'Lys'] = \
            omim.loc[
                'AMY1e AMY2e CYSTALArBATtc CYSTLEUrBATtc CYSTSERex ORNALArBATtc ORNLEUrBATtc SERLYSNaex', 'Arg'] = \
            omim.loc[
                'AMY1e AMY2e CYSTALArBATtc CYSTLEUrBATtc CYSTSERex ORNALArBATtc ORNLEUrBATtc SERLYSNaex', 'Lys'] = \
            omim.loc['ALAyLATthc GLNyLATthc HISyLATtc HISyLATthc LEUyLAThtc METyLATthc PHEyLATthc PTRCARGte SERLYSNaex', 'Arg'] = \
            omim.loc['ALAyLATthc GLNyLATthc HISyLATtc HISyLATthc LEUyLAThtc METyLATthc PHEyLATthc PTRCARGte SERLYSNaex', 'Lys'] = \
            omim.loc['FTCD GluForTx HMR_9726', 'His'] = \
            omim.loc['HISDr', 'His'] = omim.loc['CYSTS r0210', 'Met'] = omim.loc['PROD2m', 'Pro'] = \
            omim.loc['2OXOADOXm AKGDm DHRT_ibcoa GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm HMR_6397 OBDHm OIVD1m OIVD2m OIVD3m PDHm r0385 r0386 r0604 r0656 r0670', 'Leu'] = \
            omim.loc['2OXOADOXm AKGDm DHRT_ibcoa GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm HMR_6397 OBDHm OIVD1m OIVD2m OIVD3m PDHm r0385 r0386 r0604 r0656 r0670', 'Iso'] = \
            omim.loc['2OXOADOXm AKGDm DHRT_ibcoa GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm HMR_6397 OBDHm OIVD1m OIVD2m OIVD3m PDHm r0385 r0386 r0604 r0656 r0670', 'Val'] = \
            omim.loc['METAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX PHETHPTOX2', 'Phe'] = omim.loc['r0398 DHPR', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR PPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm GLYCLm THFATm', 'Gly'] = '+'
        omim.loc['PHETHPTOX PHETHPTOX2', 'Tyr'] = '-'
elif args.model == 'recon2v2':
    if not args.genes:
        omim.loc['AHC', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['CYSTSERex SERLYSNaex', 'Arg'] = omim.loc['CYSTSERex SERLYSNaex', 'Lys'] = \
            omim.loc['SERLYSNaex', 'Arg'] = omim.loc['SERLYSNaex', 'Lys'] = omim.loc['FTCD GluForTx', 'His'] = \
            omim.loc['HISD', 'His'] = omim.loc['CYSTS METS MTHFR3', 'Met'] = omim.loc['PRO1xm PROD2m', 'Pro'] = \
            omim.loc['OIVD1m OIVD2m OIVD3m', 'Leu'] = omim.loc['OIVD1m OIVD2m OIVD3m', 'Iso'] = \
            omim.loc['OIVD1m OIVD2m OIVD3m', 'Val'] = omim.loc['METAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX2 r0399', 'Phe'] = omim.loc['DHPR', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm', 'Gly'] = '+'
        omim.loc['PHETHPTOX2 r0399', 'Tyr'] = '-'
    else:
        omim.loc['AHC', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['CYSTALArBATtc CYSTLEUrBATtc CYSTSERex ORNALArBATtc ORNLEUrBATtc r0946 r1992 r1993 r1994 r1995 r1996 r1997 r1998 r1999 r2000 r2001 r2002 r2003 r2004 r2005 r2006 r2007 r2008 r2009 r2010 r2011 r2012 r2013 r2014 r2015 r2016 r2017 r2018 r2019 r2020 r2021 r2022 r2023 r2024 r2026 r2027 r2028 r2029 r2030 r2031 r2032 r2034 r2035 r2036 r2037 r2038 r2039 r2040 r2041 r2042 r2043 r2044 r2045 r2046 r2047 r2048 r2049 r2050 r2051 r2052 r2053 r2054 r2055 r2056 r2057 r2058 r2059 r2060 r2061 r2062 r2063 r2064 r2065 r2066 r2067 r2068 r2069 r2070 r2071', 'Arg'] = \
            omim.loc['CYSTALArBATtc CYSTLEUrBATtc CYSTSERex ORNALArBATtc ORNLEUrBATtc r0946 r1992 r1993 r1994 r1995 r1996 r1997 r1998 r1999 r2000 r2001 r2002 r2003 r2004 r2005 r2006 r2007 r2008 r2009 r2010 r2011 r2012 r2013 r2014 r2015 r2016 r2017 r2018 r2019 r2020 r2021 r2022 r2023 r2024 r2026 r2027 r2028 r2029 r2030 r2031 r2032 r2034 r2035 r2036 r2037 r2038 r2039 r2040 r2041 r2042 r2043 r2044 r2045 r2046 r2047 r2048 r2049 r2050 r2051 r2052 r2053 r2054 r2055 r2056 r2057 r2058 r2059 r2060 r2061 r2062 r2063 r2064 r2065 r2066 r2067 r2068 r2069 r2070 r2071', 'Lys'] = \
            omim.loc['CYSTALArBATtc CYSTLEUrBATtc CYSTSERex ORNALArBATtc ORNLEUrBATtc SERLYSNaex', 'Arg'] = \
            omim.loc['CYSTALArBATtc CYSTLEUrBATtc CYSTSERex ORNALArBATtc ORNLEUrBATtc SERLYSNaex', 'Lys'] = \
            omim.loc['ALAyLATthc GLNyLATthc HISyLATtc HISyLATthc LEUyLAThtc METyLATthc PHEyLATthc SERLYSNaex',
                     'Arg'] = \
            omim.loc['ALAyLATthc GLNyLATthc HISyLATtc HISyLATthc LEUyLAThtc METyLATthc PHEyLATthc SERLYSNaex',
                     'Lys'] = \
            omim.loc['FTCD GluForTx', 'His'] = \
            omim.loc['HISD', 'His'] = omim.loc['CYSTS r0210', 'Met'] = omim.loc['PROD2m', 'Pro'] = \
            omim.loc['2OXOADOXm AKGDm GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm OIVD1m OIVD2m OIVD3m PDHm RE3326M r0295 r0385 r0386 r0604 r0656 r0670 r1154', 'Leu'] = \
            omim.loc['2OXOADOXm AKGDm GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm OIVD1m OIVD2m OIVD3m PDHm RE3326M r0295 r0385 r0386 r0604 r0656 r0670 r1154', 'Iso'] = \
            omim.loc['2OXOADOXm AKGDm GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm OIVD1m OIVD2m OIVD3m PDHm RE3326M r0295 r0385 r0386 r0604 r0656 r0670 r1154', 'Val'] = \
            omim.loc['METAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX2 r0399', 'Phe'] = omim.loc['DHPR r0398', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm r0295', 'Gly'] = '+'
        omim.loc['PHETHPTOX2 r0399', 'Tyr'] = '-'
elif args.model == 'human1':
    if not args.genes:
        omim.loc['HMR_3877', 'Met'] = omim.loc['HMR_6774', 'Tyr'] = omim.loc['HMR_3816', 'Arg'] = \
            omim.loc[
                'HMR_5842 HMR_8687', 'Arg'] = \
            omim.loc[
                'HMR_5842 HMR_8687', 'Lys'] = \
            omim.loc[
                'HMR_5842', 'Arg'] = \
            omim.loc[
                'HMR_5842', 'Lys'] = \
            omim.loc['HMR_3929 HMR_4658', 'His'] = \
            omim.loc['HMR_4437', 'His'] = omim.loc['HMR_3879 HMR_3917 HMR_4446', 'Met'] = omim.loc[
            'HMR_3837 HMR_8611', 'Pro'] = \
            omim.loc[
                'HMR_3748 HMR_3767 HMR_3780', 'Leu'] = \
            omim.loc[
                'HMR_3748 HMR_3767 HMR_3780', 'Iso'] = \
            omim.loc[
                'HMR_3748 HMR_3767 HMR_3780', 'Val'] = \
            omim.loc['HMR_3875', 'Met'] = omim.loc['HMR_3215', 'Gly'] = \
            omim.loc['HMR_3940 HMR_8539', 'Phe'] = omim.loc['HMR_3925', 'Phe'] = omim.loc['HMR_6778', 'Met'] = \
            omim.loc['HMR_6778', 'Tyr'] = omim.loc['HMR_6772', 'Tyr'] = \
            omim.loc['HMR_6409 HMR_8434 HMR_8435 HMR_8436 HMR_8775 HMR_8778', 'Gly'] = '+'
        omim.loc['HMR_3940 HMR_8539', 'Tyr'] = '-'
    else:
        omim.loc['HMR_3877 HMR_7136', 'Met'] = omim.loc['HMR_6774', 'Tyr'] = omim.loc['HMR_3816 HMR_8426', 'Arg'] = \
            omim.loc['HMR_5842 HMR_8687', 'Arg'] = \
            omim.loc['HMR_5842 HMR_8687', 'Lys'] = \
            omim.loc['HMR_5842', 'Arg'] = \
            omim.loc['HMR_5842', 'Lys'] = \
            omim.loc['HMR_3929 HMR_4658', 'Arg'] = \
            omim.loc['HMR_3929 HMR_4658', 'Lys'] = \
            omim.loc['HMR_3929 HMR_4658 HMR_9726', 'His'] = \
            omim.loc['HMR_4437', 'His'] = omim.loc['HMR_3879 HMR_4323 HMR_7135', 'Met'] = omim.loc[
            'HMR_3838 HMR_8611', 'Pro'] = \
            omim.loc['HMR_3748 HMR_3767 HMR_3780', 'Leu'] = \
            omim.loc['HMR_3748 HMR_3767 HMR_3780', 'Iso'] = \
            omim.loc['HMR_3748 HMR_3767 HMR_3780', 'Val'] = \
            omim.loc['HMR_3875 HMR_7139', 'Met'] = omim.loc['HMR_3215', 'Gly'] = \
            omim.loc['HMR_3940 HMR_8539', 'Phe'] = omim.loc['HMR_3925 HMR_4523', 'Phe'] = omim.loc['HMR_6778', 'Met'] = \
            omim.loc['HMR_6778', 'Tyr'] = omim.loc['HMR_6726 HMR_6772', 'Tyr'] = \
            omim.loc['HMR_3923 HMR_4665 HMR_6409 HMR_8433 HMR_8434 HMR_8435 HMR_8436 HMR_8437', 'Gly'] = '+'
        omim.loc['HMR_3940 HMR_8539', 'Tyr'] = '-'

# omim.to_csv("truth_2.csv")
# data.to_csv("data_2.csv")
tp = 0
fp = 0
fn = 0


for index, row in data.iterrows():
    for colname, item in data.loc[index].iteritems():
        if (item == '+1' and omim.loc[index, colname] == '+') or (item == '-1' and omim.loc[index, colname] == '-'):
            tp += 1
        if (item == '+1' and pd.isna(omim.loc[index, colname]))\
                or (item == '-1' and pd.isna(omim.loc[index, colname])):
            fp += 1
        if (item == '00' and omim.loc[index, colname] == '+') or (item == '00' and omim.loc[index, colname] == '-'):
            fn += 1

if tp + fp == 0:
    precision = 0
else:
    precision = tp / (tp + fp)
if tp + fn == 0:
    recall = 0
else:
    recall = tp / (tp + fn)
print(os.path.basename(args.input))
print("Precision: "+str(round(precision, 2)))
print("Recall: "+str(round(recall, 2)))
print("Number of TP: "+str(tp))
omim.to_csv("out/truth.csv")
data.to_csv("out/data.csv")