import sys

import pandas as pd
import argparse
from argparse import RawTextHelpFormatter
import time
import re
import cobra
from pathlib import Path


def unique(L):
    seen = set()
    for item in L:
        if item not in seen:
            seen.add(item)
            yield item


description = "Imports a FVA csv results file and makes a coloured table png.\n"

parser = argparse.ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)
parser.add_argument("-i", "--input", help="Input csv table filename")
parser.add_argument("-o", "--output", help="Output filename prefix")
parser.add_argument("-s", "--shlomi", action="store_true", help="Use this flag to recreate Shlomi's table")
parser.add_argument("-S", "--addshlomi", action="store_true",
                    help="Use this flag to only add Shlomi's OMIM +'s and -'s")
parser.add_argument("-r", "--recon", choices=['recon1', 'recon2v2', 'recon3d', 'human1'], help="Recon version")
parser.add_argument("-f", "--format", choices=['html', 'png'], default='html', help="Output file format")
parser.add_argument("-n", "--names", action="store_true", help="Use this flag to show the metabolite names")
parser.add_argument("-c", "--colours", action="store_true", help="Use this flag to use different colours for (++) or "
                                                                 "(+) etc.")
parser.add_argument("-w", "--writedata", action="store_true", help="Use this flag to write the data table to csv")
parser.add_argument("-W", "--onlywritedata", action="store_true", help="Use this flag to write the data table to csv, "
                                                                       "and not output a png or html file")
parser.add_argument("-u", "--uniqueids", action="store_true", help="Use this flag to use and write unique IDs instead "
                                                                   "of names")
parser.add_argument("--omimextra", action="store_true", help="Use this flag to add extra OMIM flags")
parser.add_argument("--convertids", action="store_true", help="Use this flag to convert ids to understandable "
                                                                "metabolite names (requires original SBML model)")
parser.add_argument("--model", default=None, help="The metabolic model for ID to name conversion")
args = parser.parse_args()
if args.format == 'png':
    import imgkit
    from xvfbwrapper import Xvfb

start_time = time.time()

in_table = pd.read_csv(args.input)
all_biomarkers = []
if not args.shlomi:
    for bm in in_table['Biomarkers'][in_table['Biomarkers'].notnull()]:
        only_names = []
        if not args.uniqueids:
            # Split based on (+) or (++) or (-) or (--) or (?)
            temp = re.split(r"( \([+-?]{1,2}\))", bm)
            # Fix parsing:
            for i in range(len(temp)):
                if temp[i].startswith(', '):
                    temp[i] = temp[i][2:]
            for i in range(len(temp)):
                if not temp[i].startswith(' ') and temp[i] != '':
                    only_names.append(temp[i])
        else:
            temp = bm.split('; ')
            for item in temp:
                only_names.append(item.split(' # ')[0])
        all_biomarkers.append(only_names)

    # Split all the biomarkers:
    # if not args.uniqueids:
    all_biomarkers_split = [item for sublist in all_biomarkers for item in sublist]
    all_biomarkers_split[:] = unique(all_biomarkers_split)
    # else:
    #     all_biomarkers_split = all_biomarkers
    #     # print(all_biomarkers)
    # This list of biomarkers will become the column names for the final table
else:
    if not args.uniqueids:
        all_biomarkers_split = ['L-Arginine', 'L-Leucine', 'L-Phenylalanine', 'L-Cysteine', 'L-Glutamine', 'L-Serine',
                                'L-Asparagine', 'L-Tryptophan', 'L-Proline', 'L-Threonine', 'L-Aspartate', 'Glycine',
                                'L-Glutamate', 'L-Isoleucine', 'L-Lysine', 'L-Valine', 'L-Methionine', 'L-Tyrosine',
                                'L-Alanine', 'L-Histidine']

    else:
        for bm in in_table['Biomarkers'][in_table['Biomarkers'].notnull()]:
            only_names = []
            temp = bm.split('; ')
            for item in temp:
                only_names.append(item.split(' # ')[0])
            all_biomarkers.append(only_names)
        all_biomarkers_split = [item for sublist in all_biomarkers for item in sublist]
        all_biomarkers_split[:] = unique(all_biomarkers_split)
        if len(all_biomarkers_split) != 20:
            if args.recon == 'recon2v2':
                aa_dict = {'cys_L_e': 'L-Cysteine', 'phe_L_e': 'L-Phenylalanine', 'tyr_L_e': 'L-Tyrosine',
                           'his_L_e': 'L-Histidine',
                           'met_L_e': 'L-Methionine', 'ile_L_e': 'L-Isoleucine', 'leu_L_e': 'L-Leucine',
                           'lys_L_e': 'L-Lysine',
                           'thr_L_e': 'L-Threonine', 'trp_L_e': 'L-Tryptophan', 'val_L_e': 'L-Valine',
                           'arg_L_e': 'L-Arginine',
                           'gln_L_e': 'L-Glutamine', 'asn_L_e': 'L-Asparagine', 'glu_L_e': 'L-Glutamate',
                           'gly_e': 'Glycine',
                           'ser_L_e': 'L-Serine', 'pro_L_e': 'L-Proline', 'asp_L_e': 'L-Aspartate', 'ala_L_e': 'L-Alanine'
                           }
            elif args.recon == 'recon3d' or args.recon == 'recon1':
                aa_dict = {'cys__L_e': 'L-Cysteine', 'phe__L_e': 'L-Phenylalanine', 'tyr__L_e': 'L-Tyrosine',
                           'his__L_e': 'L-Histidine',
                           'met__L_e': 'L-Methionine', 'ile__L_e': 'L-Isoleucine', 'leu__L_e': 'L-Leucine',
                           'lys__L_e': 'L-Lysine',
                           'thr__L_e': 'L-Threonine', 'trp__L_e': 'L-Tryptophan', 'val__L_e': 'L-Valine',
                           'arg__L_e': 'L-Arginine',
                           'gln__L_e': 'L-Glutamine', 'asn__L_e': 'L-Asparagine', 'glu__L_e': 'L-Glutamate',
                           'gly_e': 'Glycine',
                           'ser__L_e': 'L-Serine', 'pro__L_e': 'L-Proline', 'asp__L_e': 'L-Aspartate',
                           'ala__L_e': 'L-Alanine'
                           }
            elif args.recon == 'human1':
                aa_dict = {'cysteine': 'L-Cysteine', 'phenylalanine': 'L-Phenylalanine', 'tyrosine': 'L-Tyrosine',
                           'histidine': 'L-Histidine',
                       'methionine': 'L-Methionine', 'isoleucine': 'L-Isoleucine', 'leucine': 'L-Leucine', 'lysine': 'L-Lysine',
                       'threonine': 'L-Threonine', 'tryptophan': 'L-Tryptophan', 'valine': 'L-Valine', 'arginine': 'L-Arginine',
                       'glutamine': 'L-Glutamine', 'asparagine': 'L-Asparagine', 'glutamate': 'L-Glutamate', 'glycine': 'Glycine',
                       'serine': 'L-Serine', 'proline': 'L-Proline', 'aspartate': 'L-Aspartate', 'alanine': 'L-Alanine'}
            for aa in aa_dict.keys():
                if aa not in all_biomarkers_split:
                    all_biomarkers_split.append(aa)
            # for i, aa in enumerate(all_biomarkers_split):
            #     all_biomarkers_split[i] = aa_dict[aa]

data = pd.DataFrame(columns=['Reaction ID', 'Biomarkers'] + all_biomarkers_split)
data['Reaction ID'], data['Biomarkers'] = in_table['Reaction ID'], in_table['Biomarkers']
data.set_index('Reaction ID', inplace=True)

if not data.isnull().all().all():  # Check if not completely empty
    for rxn, row in data.iterrows():
        bm_list = []
        if type(data.loc[[rxn], ['Biomarkers']].values[0][0]) != float:
            if not args.uniqueids:
                bm_list = re.split(r"( \([+-?]{1,2}\)(?!\-))", data.loc[[rxn], ['Biomarkers']].values[0][0])
                bm_list = [''.join(x) for x in zip(bm_list[0::2], bm_list[1::2])]
                for i in range(len(bm_list)):
                    if bm_list[i].startswith(', '):
                        bm_list[i] = bm_list[i][2:]
            else:
                bm_list = (data.loc[[rxn], ['Biomarkers']].values[0][0]).split('; ')
        else:
            bm_list = ['']
        change_list = []
        for i, bm in enumerate(bm_list):
            if bm != '':
                if not args.uniqueids:
                    if bm.endswith('(-)') or bm.endswith('(+)') or bm.endswith('(?)'):
                        if bm.endswith('(-)'):
                            change_list.append("-1")
                        elif bm.endswith('(+)'):
                            change_list.append("+1")
                        else:
                            change_list.append('00')
                        bm_list[i] = bm[:-4]
                    else:  # Can be equal to (--) or (++)
                        if bm.endswith('(--)'):
                            change_list.append("-1")
                        elif bm.endswith('(++)'):
                            change_list.append("+1")
                        bm_list[i] = bm[:-5]
                else:
                    bm_list[i] = bm.split(' # ')[0]
                    if (bm.split(' # ')[1]) == '(-)':
                        change_list.append("-1")
                    elif (bm.split(' # ')[1]) == '(+)':
                        change_list.append("+1")
                    # Can be equal to (--) or (++)
                    elif (bm.split(' # ')[1]) == '(--)':
                        change_list.append("-1")
                    elif (bm.split(' # ')[1]) == '(++)':
                        change_list.append("+1")
                    else:
                        change_list.append('00')

        for i, bm in enumerate(bm_list):
            if bm != '':
                for column in data.columns[1:]:
                    if column.casefold() == bm.casefold():
                        data.at[rxn, column] = change_list[i]

data.drop('Biomarkers', 1, inplace=True)
data.fillna("00", inplace=True)

if args.shlomi:
    if args.recon == 'recon2v2':
        aa_tri_dict = {'cys_L_e': 'Cys', 'phe_L_e': 'Phe', 'tyr_L_e': 'Tyr', 'his_L_e': 'His',
                       'met_L_e': 'Met', 'ile_L_e': 'Iso', 'leu_L_e': 'Leu', 'lys_L_e': 'Lys',
                       'thr_L_e': 'Thr', 'trp_L_e': 'Trp', 'val_L_e': 'Val', 'arg_L_e': 'Arg',
                       'gln_L_e': 'Gln', 'asn_L_e': 'Asn', 'glu_L_e': 'Glu', 'gly_e': 'Gly',
                       'ser_L_e': 'Ser', 'pro_L_e': 'Pro', 'asp_L_e': 'Asp', 'ala_L_e': 'Ala'
                       }
    elif args.recon == 'recon3d' or args.recon == 'recon1':
        aa_tri_dict = {'cys__L_e': 'Cys', 'phe__L_e': 'Phe', 'tyr__L_e': 'Tyr', 'his__L_e': 'His',
                       'met__L_e': 'Met', 'ile__L_e': 'Iso', 'leu__L_e': 'Leu', 'lys__L_e': 'Lys',
                       'thr__L_e': 'Thr', 'trp__L_e': 'Trp', 'val__L_e': 'Val', 'arg__L_e': 'Arg',
                       'gln__L_e': 'Gln', 'asn__L_e': 'Asn', 'glu__L_e': 'Glu', 'gly_e': 'Gly',
                       'ser__L_e': 'Ser', 'pro__L_e': 'Pro', 'asp__L_e': 'Asp', 'ala__L_e': 'Ala'
                       }
    elif args.recon == 'human1':
        aa_tri_dict = {'cysteine': 'Cys', 'phenylalanine': 'Phe', 'tyrosine': 'Tyr', 'histidine': 'His',
                       'methionine': 'Met', 'isoleucine': 'Iso', 'leucine': 'Leu', 'lysine': 'Lys',
                       'threonine': 'Thr', 'tryptophan': 'Trp', 'valine': 'Val', 'arginine': 'Arg',
                       'glutamine': 'Gln', 'asparagine': 'Asn', 'glutamate': 'Glu', 'glycine': 'Gly',
                       'serine': 'Ser', 'proline': 'Pro', 'aspartate': 'Asp', 'alanine': 'Ala'}
    for aa in data.columns:
        data.rename(columns={aa: aa_tri_dict[aa]}, inplace=True)

    column_order = ['Arg', 'Leu', 'Phe', 'Cys', 'Gln', 'Ser',
                    'Asn', 'Trp', 'Pro', 'Thr', 'Asp', 'Gly',
                    'Glu', 'Iso', 'Lys', 'Val', 'Met', 'Tyr',
                    'Ala', 'His']
    data = data[column_order]
elif args.addshlomi:
    aas = ['L-Arginine', 'L-Leucine', 'L-Phenylalanine', 'L-Cysteine', 'L-Glutamine', 'L-Serine',
           'L-Asparagine', 'L-Tryptophan', 'L-Proline', 'L-Threonine', 'L-Aspartate', 'Glycine',
           'L-Glutamate', 'L-Isoleucine', 'L-Lysine', 'L-Valine', 'L-Methionine', 'L-Tyrosine',
           'L-Alanine', 'L-Histidine']
    aas_short = ['Arg', 'Leu', 'Phe', 'Cys', 'Gln', 'Ser',
                 'Asn', 'Trp', 'Pro', 'Thr', 'Asp', 'Gly',
                 'Glu', 'Iso', 'Lys', 'Val', 'Met', 'Tyr',
                 'Ala', 'His']
    for i, metab in enumerate(data.columns):
        for j, aa in enumerate(aas):
            if metab.casefold() == aa.casefold():
                data.rename(columns={data.columns[i]: aas_short[j]}, inplace=True)
pd.set_option('display.max_columns', 22)

if args.convertids:
    fileformat = Path(args.model).suffix
    if fileformat == '.sbml' or fileformat == '.xml':
        print('Detected sbml model...')
        model = cobra.io.read_sbml_model(args.model)
    elif fileformat == '.json':
        print('Detected json model...')
        model = cobra.io.load_json_model(args.model)
    else:
        print('Please use a json or sbml file as your model.')
    for i, metab in enumerate(data.columns):
        newname = model.metabolites.get_by_id(metab).name + metab[-2:]  # Add compartment for unique names
        data.rename(columns={data.columns[i]: newname}, inplace=True)  # Rename columns
    data = data.reindex(sorted(data.columns), axis=1)
# Add conditional background colours:
if args.colours:
    def background_function(s):
        colors = {"-1": '#ff5c5c', "-2": '##de2a2a', "+1": '#547bf0', "+2": '#254aba'}
        return data.applymap(lambda val: 'background-color: {}'.format(colors.get(val, '')))
else:
    def background_function(s):
        colors = {"-1": '#FF4E4E', "-2": '#FF4E4E', "+1": '#4169E1', "+2": '#4169E1'}
        return data.applymap(lambda val: 'background-color: {}'.format(colors.get(val, '')))


def text_colour(s):
    return ['color: rgba(0, 0, 0, 0)' for v in s]


omim = pd.DataFrame().reindex_like(data)
if args.shlomi or args.addshlomi:
    if args.recon == 'recon1':
        omim.loc['AHCi', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['CYSTSERex SERLYSNaex', 'Arg'] = omim.loc['CYSTSERex SERLYSNaex', 'Lys'] = \
            omim.loc['SERLYSNaex', 'Arg'] = omim.loc['SERLYSNaex', 'Lys'] = omim.loc['FTCD GluForTx', 'His'] = \
            omim.loc['HISDr', 'His'] = omim.loc['CYSTS MTHFR3 METS', 'Met'] = omim.loc['PRO1xm PROD2m', 'Pro'] = \
            omim.loc['OIVD2m OIVD1m OIVD3m', 'Leu'] = omim.loc['OIVD2m OIVD1m OIVD3m', 'Iso'] = \
            omim.loc['OIVD2m OIVD1m OIVD3m', 'Val'] = omim.loc['METAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX2', 'Phe'] = omim.loc['DHPR', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm', 'Gly'] = '+'
        omim.loc['PHETHPTOX2', 'Tyr'] = '-'
    elif args.recon == 'recon3d':
        omim.loc['AHCi', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['CYSTSERex SERLYSNaex', 'Arg'] = omim.loc['CYSTSERex SERLYSNaex', 'Lys'] = \
            omim.loc['SERLYSNaex', 'Arg'] = omim.loc['SERLYSNaex', 'Lys'] = omim.loc['FTCD GluForTx', 'His'] = \
            omim.loc['HISDr', 'His'] = omim.loc['CYSTS MTHFR3 METS', 'Met'] = omim.loc['PRO1x PROD2m', 'Pro'] = \
            omim.loc['OIVD2m OIVD1m OIVD3m', 'Leu'] = omim.loc['OIVD2m OIVD1m OIVD3m', 'Iso'] = \
            omim.loc['OIVD2m OIVD1m OIVD3m', 'Val'] = omim.loc['METAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX2', 'Phe'] = omim.loc['DHPR', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm', 'Gly'] = '+'
        omim.loc['PHETHPTOX2', 'Tyr'] = '-'
    elif args.recon == 'recon2v2':
        omim.loc['AHC', 'Met'] = omim.loc['HGNTOR', 'Tyr'] = omim.loc['ARGN', 'Arg'] = \
            omim.loc['CYSTSERex SERLYSNaex', 'Arg'] = omim.loc['CYSTSERex SERLYSNaex', 'Lys'] = \
            omim.loc['SERLYSNaex', 'Arg'] = omim.loc['SERLYSNaex', 'Lys'] = omim.loc['FTCD GluForTx', 'His'] = \
            omim.loc['HISD', 'His'] = omim.loc['CYSTS MTHFR3 METS', 'Met'] = omim.loc['PRO1xm PROD2m', 'Pro'] = \
            omim.loc['OIVD2m OIVD1m OIVD3m', 'Leu'] = omim.loc['OIVD2m OIVD1m OIVD3m', 'Iso'] = \
            omim.loc['OIVD2m OIVD1m OIVD3m', 'Val'] = omim.loc['METAT', 'Met'] = omim.loc['MMMm', 'Gly'] = \
            omim.loc['PHETHPTOX2 r0399', 'Phe'] = omim.loc['DHPR', 'Phe'] = omim.loc['FUMAC', 'Met'] = \
            omim.loc['FUMAC', 'Tyr'] = omim.loc['34HPPOR', 'Tyr'] = \
            omim.loc['GCC2am GCC2bim GCC2cm GCCam GCCbim GCCcm', 'Gly'] = '+'
        omim.loc['PHETHPTOX2 r0399', 'Tyr'] = '-'
        if args.omimextra:
            omim.loc['AHC', 'S-adenosyl-L-homocysteine'] = omim.loc['HGNTOR', 'homogentisate'] = \
                omim.loc['HGNTOR', 'Phe'] = omim.loc['CYSTSERex SERLYSNaex', 'Lys'] = \
                omim.loc['CYSTSERex SERLYSNaex', 'Arg'] = omim.loc['SERLYSNaex', 'ornithine'] = \
                omim.loc['SERLYSNaex', 'Lys'] = \
                omim.loc['CYSTSERex SERLYSNaex', 'L-cystine'] = omim.loc['CYSTSERex SERLYSNaex', 'ornithine'] = \
                omim.loc['SERLYSNaex', 'ornithine'] = omim.loc['CYSTS MTHFR3 METS', 'S-adenosyl-L-homocysteine'] = \
                omim.loc['PRO1xm PROD2m', 'trans-4-hydroxy-L-proline'] = omim.loc['PRO1xm PROD2m', 'Gly'] = \
                omim.loc['METAT', 'S-adenosyl-L-methionine'] = omim.loc['MMMm', '(S)-methylmalonyl-CoA(5-)'] = '+'
            omim.loc['ARGN', 'urea'] = omim.loc['SERLYSNaex', 'urea'] = '-'
    elif args.recon == 'human1':
        omim.loc['HMR_3877', 'Met'] = omim.loc['HMR_6774', 'Tyr'] = omim.loc['HMR_3816', 'Arg'] = \
            omim.loc['HMR_8687 HMR_5842', 'Arg'] = omim.loc['HMR_8687 HMR_5842', 'Lys'] = \
            omim.loc['HMR_5842', 'Arg'] = omim.loc['HMR_5842', 'Lys'] = omim.loc['HMR_3929 HMR_4658', 'His'] = \
            omim.loc['HMR_4437', 'His'] = omim.loc['HMR_3879 HMR_4446 HMR_3917', 'Met'] = omim.loc['HMR_3837 HMR_8611', 'Pro'] = \
            omim.loc['HMR_3748 HMR_3767 HMR_3780', 'Leu'] = omim.loc['HMR_3748 HMR_3767 HMR_3780', 'Iso'] = \
            omim.loc['HMR_3748 HMR_3767 HMR_3780', 'Val'] = omim.loc['HMR_3875', 'Met'] = omim.loc['HMR_3215', 'Gly'] = \
            omim.loc['HMR_8539 HMR_3940', 'Phe'] = omim.loc['HMR_3925', 'Phe'] = omim.loc['HMR_6778', 'Met'] = \
            omim.loc['HMR_6778', 'Tyr'] = omim.loc['HMR_6772', 'Tyr'] = \
            omim.loc['HMR_8775 HMR_8778 HMR_6409 HMR_8434 HMR_8436 HMR_8435', 'Gly'] = '+'
        omim.loc['HMR_8539 HMR_3940', 'Tyr'] = '-'
else:
    omim = data.copy()

diff = len(omim.columns) - len(data.columns)
if diff != 0:
    for i in range(diff):
        omim.drop(omim.columns[len(omim.columns) - 1], axis=1, inplace=True)

if args.writedata:
    omim.to_csv(args.output + ".csv")
if args.onlywritedata:
    omim.to_csv(args.output + ".csv")
    sys.exit(0)

omimstyle = omim.style

omimstyle.apply(background_function, axis=None)  # Colour cells based on (+1) or (-1)
if not (args.shlomi or args.addshlomi):
    omimstyle.apply(text_colour)
omimstyle.set_properties(
    **{'max-width': '15px', 'border-width': '1px', 'border-color': 'black', 'border-style': 'solid',
       'text-align': 'center', 'font-size': '20px', 'font-family': 'monospace', 'white-space': 'pre'})
# Make the column names diagonal:
if not args.names:
    omimstyle.set_table_styles(
        [dict(selector="th.col_heading",
              props=[('display', "none")]),
         dict(selector="th.row_heading",
              props=[('font-family', 'sans-serif'), ('font-size', '12px'), ('line-height', '12px')])])
else:
    omimstyle.set_table_styles(
        [dict(selector="th.col_heading",
              props=[('vertical-align', 'bottom'), ('font-family', 'monospace'), ('max-width', '90px'),
                     ('transform', 'rotateZ(-45deg)')]),
         dict(selector="th.row_heading",
              props=[('font-family', 'sans-serif'), ('font-size', '14px'), ('line-height', '20px')])])
# print(omim)

html = omimstyle.render().replace('nan', ' ')

if args.format == 'html':
    with open(args.output + ".html", 'w') as outfile:
        outfile.write(html)
elif args.format == 'png':
    vdisplay = Xvfb()
    vdisplay.start()
    imgkit.from_string(html, args.output + ".png")
elapsed_time = time.time() - start_time
print("Total elapsed time: {:.2f} sec".format(elapsed_time))
