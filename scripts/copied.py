# Functions copied from various sources


# Taken from unimplemented (?) cobrapy code:
def parse_legacy_sbml_notes(note_string, note_delimiter=':'):
    """Deal with various legacy SBML format issues.
    """
    note_dict = {}
    start_tag = '<p>'
    end_tag = '</p>'
    if '<html:p>' in note_string:
        start_tag = '<html:p>'
        end_tag = '</html:p>'
    while start_tag in note_string and end_tag in note_string:
        note_start = note_string.index(start_tag)
        note_end = note_string.index(end_tag)
        the_note = note_string[
                   (note_start + len(start_tag)):note_end].lstrip(' ').rstrip(
            ' ')
        if note_delimiter in the_note:
            note_delimiter_index = the_note.index(note_delimiter)
            note_field = the_note[:note_delimiter_index].lstrip(
                ' ').rstrip(' ').replace('_', ' ').upper()
            note_value = the_note[
                         (note_delimiter_index + 1):].lstrip(' ').rstrip(' ')
            if note_field in note_dict:
                note_dict[note_field].append(note_value)
            else:
                note_dict[note_field] = [note_value]
        note_string = note_string[(note_end + len(end_tag)):]

    if ('CHARGE' in note_dict and
            note_dict['CHARGE'][0].lower() in ['none', 'na', 'nan']):
        note_dict.pop('CHARGE')  # Remove non-numeric charges

    if 'CHARGE' in note_dict and note_dict['CHARGE'][0].lower() in ['none',
                                                                    'na',
                                                                    'nan']:
        note_dict.pop('CHARGE')  # Remove non-numeric charges

    return note_dict


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()
