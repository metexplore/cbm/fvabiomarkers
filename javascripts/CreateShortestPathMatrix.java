package test1;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioMetabolite;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioReactant;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_graph.computation.algo.ShortestPath;
import fr.inrae.toulouse.metexplore.met4j_graph.core.BioPath;
import fr.inrae.toulouse.metexplore.met4j_graph.core.compound.CompoundGraph;
import fr.inrae.toulouse.metexplore.met4j_graph.core.compound.ReactionEdge;
import fr.inrae.toulouse.metexplore.met4j_graph.io.Bionetwork2BioGraph;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.JsbmlReader;
import org.jgrapht.traverse.BreadthFirstIterator;
import test1.ShortestPathCustom;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class CreateShortestPathMatrix {
    public static void main(String[] args) throws Exception {
        // Import model
        String inputFile = "D:/Users/Juliette/Documents/StageM2/Code/data/Recon2.2_reimported2.xml";
        JsbmlReader reader = new JsbmlReader(inputFile);
        // Convert into BioNetwork
        BioNetwork network = reader.read();

//        // Read side compounds file
//        Scanner sc = new Scanner(new File("D:/Users/Juliette/Documents/StageM2/Code/met4jtest/newtest/data/side_compounds.txt"));
//        List<String> lines = new ArrayList<>();
//        while (sc.hasNextLine()) {
//            lines.add(sc.nextLine());
//        }
//        String[] sideComp = lines.toArray(new String[0]);
//
//        // Convert into temporary BioGraph then CompoundGraph (to extract side compound metabolites)
//        Bionetwork2BioGraph builder = new Bionetwork2BioGraph(network);
//        CompoundGraph tempG = builder.getCompoundGraph();
//
//        // Create BioCollection containing side compounds
//        BioCollection sideCompounds = new BioCollection();
//        for (String s : sideComp) {
//            BioMetabolite tempMetab = tempG.getVertex(s);
//            if (tempMetab != null) {
//                sideCompounds.add(tempMetab);
//            }
//        }
//
//        // Remove side compounds from BioNetwork
//        network.removeOnCascade(sideCompounds);

        // Rebuild graph from new BioNetwork
        Bionetwork2BioGraph newBuilder = new Bionetwork2BioGraph(network);
        CompoundGraph g = newBuilder.getCompoundGraph();




        // Read the FVA matrix "D:\\Users\\Juliette\\Documents\\StageM2\\results\\server_all\\new_ids\\table.csv"
        // Find out how to get ncol and nrow automatically
        int ncol = 576;
        int nrow = 1471;
        // create a reader
        int i = 0;
        String[][] fvaTable = new String[nrow][ncol];
        try (BufferedReader br = Files.newBufferedReader(Paths.get("D:\\Users\\Juliette\\Documents\\StageM2\\results\\server_all\\new_ids\\table_no_side_compounds.csv"))) {
            // CSV file delimiter
            String DELIMITER = ",";
            // read the file line by line
            String line;
            while ((line = br.readLine()) != null) {

                // convert line into columns and store it
                String[] columns = line.split(DELIMITER);
                for (int j = 0; j < ncol; j++) {
                    fvaTable[i][j] = columns[j];
                }
                i++;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        // Create the distance matrix as a copy of the fvaTable
        String[][] distTable = new String[nrow][ncol];
        for (int k = 0; k < nrow; k++) {
            distTable[k] = Arrays.copyOf(fvaTable[k], fvaTable[k].length);
        }

//        // Test for 1 reaction + metab
//        // Target metabolite
//        String nthMet = "M_acgalfucgalacgalfuc12gal14acglcgalgluside_hs_e";
//        BioMetabolite met = g.getVertex(nthMet);
//
//        // We can only calculate shortest path from metabolite to metabolite
//        // So we need to test all the products of a reaction
//        // Target reaction
//        String mthRxn = "R_RE2680C";
//        HashSet<ReactionEdge> reactions = g.getEdgesFromReaction(mthRxn);
//        Iterator<ReactionEdge> iterator = reactions.iterator();
//        if (iterator.hasNext()) {
//            ReactionEdge rxn = iterator.next();
//
//            // Get all reaction reactants:
//            BioCollection<BioReactant> rxnReactants = rxn.getReaction().getReactantsView();
//            Iterator<BioReactant> bioReactantIterator = rxnReactants.iterator();
//            List<BioReactant> reactantsList = new ArrayList<>();
//            while (bioReactantIterator.hasNext()) {
//                reactantsList.add(bioReactantIterator.next());
//            }
//            // Remove side compounds (may not be necessary - to be tested)
////            bioReactantIterator = rxnProducts.iterator();
////            while (bioReactantIterator.hasNext()) {
////                BioReactant metab = bioReactantIterator.next();
////                String metabName = metab.getMetabolite().getId();
////                for (String s : sideComp) {
////                    if (s.equalsIgnoreCase(metabName)) {
////                        productsList.remove(metab);
////                    }
////                }
////            }
//
//            // Check path length for each product
//            int min = 10000; // Arbitrary initialization
//            int out = 0;
//            BioPath<BioMetabolite, ReactionEdge> minPath = null;
//            BioPath<BioMetabolite, ReactionEdge> path;
//            int ind = 0;
//            ShortestPathCustom<BioMetabolite, ReactionEdge, CompoundGraph> pathSearch = new ShortestPathCustom<>(g);
//            for (BioReactant bioReactant : reactantsList) {
//                BioMetabolite reactionMetab = bioReactant.getMetabolite();
//                // If the metab is a product
////                System.out.println(rxn.getReaction().getRightReactantsView());
////                System.out.println(reactionMetab);
////                System.out.println(rxn.getReaction().getRightReactantsView().containsId(reactionMetab.getId()));
//                if (rxn.getReaction().getRightReactantsView().containsId(reactionMetab.getId())){
//                    path = pathSearch.getShortestAsUndirected(reactionMetab, met);
//                }
//                // If the metab is an input metabolite
//                else {
//                    path = pathSearch.getShortestAsUndirected(met, reactionMetab);
//                }
//                System.out.println(path);
//                if (ind == 0) {  // Get the first path to set it as min
//                    if (path == null) {
//                        if (met == reactionMetab) {
//                            min = 1;
//                        }
//                        else{
//                            out = -1;
//                        }
//                    } else {
//                        min = path.getEdgeList().size();
//                        minPath = path;
//                    }
//                } else {
//                    if (path == null) {
//                        // The target metabolite is the same as the source metabolite, so path is null:
//                        if (met == reactionMetab) {
//                            min = 1;
//                        }
//                        else{
//                            out = -1;
//                        }
//
//                    } else {
//                        if (path.getEdgeList().size() <= min) {
//                            min = path.getEdgeList().size();
//                            minPath = path;
//                        }
//                    }
//
//                }
//                ind++;
//            }
//            System.out.println(out);
//            System.out.println(min);
//            System.out.println(minPath);
//        }


        // Create loop here
        for (int m = 1; m < nrow; m++) {
//            System.out.println(m);
//            System.out.println(fvaTable[m][0]);
            for (int n = 1; n < ncol; n++) {
                // If the KO of this reaction affects this metabolite:
                if (!fvaTable[m][n].equals("0")) {

                    // Add no-distance value instead of 0
                    // Target metabolite
                    String nthMet = "M_" + fvaTable[0][n];
                    BioMetabolite met = g.getVertex(nthMet);

                    // We can only calculate shortest path from metabolite to metabolite
                    // So we need to test all the products of a reaction
                    // Target reaction
                    String mthRxn = "R_" + fvaTable[m][0];
                    HashSet<ReactionEdge> reactions = g.getEdgesFromReaction(mthRxn);
                    Iterator<ReactionEdge> iterator = reactions.iterator();
                    if (iterator.hasNext()) {
                        ReactionEdge rxn = iterator.next();

                        // Get reaction products:
                        BioCollection<BioReactant> rxnReactants = rxn.getReaction().getReactantsView();
                        Iterator<BioReactant> bioReactantIterator = rxnReactants.iterator();
                        List<BioReactant> reactantsList = new ArrayList<>();
                        while (bioReactantIterator.hasNext()) {
                            reactantsList.add(bioReactantIterator.next());
                        }

                        // Check path length for each product
                        int min = 10000; // Arbitrary initialization
                        int out = 0;
                        BioPath<BioMetabolite, ReactionEdge> minPath = null;
                        BioPath<BioMetabolite, ReactionEdge> path;
                        int ind = 0;
                        ShortestPath<BioMetabolite, ReactionEdge, CompoundGraph> pathSearch = new ShortestPath<>(g);
                        for (BioReactant bioReactant : reactantsList) {
                            BioMetabolite reactionMetab = bioReactant.getMetabolite();
                            // If the metab is a product
                            if (rxn.getReaction().getRightReactantsView().containsId(reactionMetab.getId())){
                                path = pathSearch.getShortestAsUndirected(reactionMetab, met);
                            }
                            // If the metab is an input metabolite
                            else {
                                path = pathSearch.getShortestAsUndirected(met, reactionMetab);
                            }
                            if (ind == 0) {  // Get the first path to set it as min
                                if (path == null) {
                                    if (met == reactionMetab) {
                                        min = 1;
                                    }
                                    else{
                                        out = -1;
                                    }

                                } else {
                                    min = path.getEdgeList().size()+1;
//                                    minPath = path;
                                }
                            } else {
                                if (path == null) {
                                    if (met == reactionMetab) {
                                        min = 1;
                                    }
                                    else{
                                        out = -1;
                                    }
                                } else {
                                    if (path.getEdgeList().size() <= min) {
                                        min = path.getEdgeList().size()+1;
//                                        minPath = path;
                                    }
                                }

                            }
                            ind++;
                        }
                        if (min != 10000) {
                            distTable[m][n] = Integer.toString(min);
                        }
                        else {
                            distTable[m][n] = Integer.toString(out);
                        }

                    }
                }
//                   System.out.println("Reaction: "+fvaTable[m][0]+" Metabolite: "+fvaTable[0][n]+" Minimum path: "+min);
            }
//            System.out.println(Arrays.toString(distTable[m])+"\n");
        }

        // Write file to CSV
        FileWriter csvWriter = new FileWriter("D:/Users/Juliette/Documents/StageM2/Code/met4jtest/newtest/data/reruns/distance_table_undirected_with_sc.csv");
        for(String[] rows : distTable){
            csvWriter.append(String.join(",", rows));
            csvWriter.append("\n");
        }
        csvWriter.flush();
        csvWriter.close();
    }
}